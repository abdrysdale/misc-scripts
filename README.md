# Miscellaneous Scripts

This repository is a home for all those scripts that are too small to warrant their own project but are non-trivial enough to warrant re-use.

If adding your own scripts, please ensure that the dependencies for the script are kept minimal and any dependencies are explicitly mentioned along with a simple usage example. 

This README will serve as an index for the scripts.

## Scripts

### LLM (large language model)

#### sumabs

Uses ollama with llama2 to summarise abstracts from a url.
Currently only tested on linux, should work with any UNIX based OS that has [wget](https://www.gnu.org/software/wget/) and [ollama](https://ollama.ai/) installed.

Different llm models can be used by changing the `run_llm_from_prompt` function.

Usage info:

```
Summarises abstracts from urls.

Usage:
        sumabs [url, urls or file]

Examples:

        sumabs https://my.paper.url # Will summarise the abstract and print results to the screen.
        sumabs www.url-1.com www.url-2.com # Will summarise the abstract of each url.
        sumabs path/to/url/file # Expects each url to be separated by a new line.

Requirements:
        - wget
        - ollama (with llama2)

Flags:
        -h, --help      Prints this message.
```
